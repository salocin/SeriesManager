
import re

class RegexManager:

    # TODO use the series_regex for series number detection instead of number regex
    series__number_regex = re.compile(r's\d\d', re.IGNORECASE) # format s01
    leading_zeros = re.compile(r'\b0+(\d)') # needed to make this Season 01 to Season 1
    series_numbering_format_1 = re.compile(r's\d\de\d\d', re.IGNORECASE)  # format: s01e04
    series_numbering_format_2 = re.compile(r'\d\dx\d\d', re.IGNORECASE) # format: 01x03
    series_numbering_format_3 = re.compile(r'\d\d\d', re.IGNORECASE) # needed?
