import os
import re
import shutil
from collections import defaultdict
from SeriesManager.regexes import RegexManager



class SeriesManager(object):

    def __init__(self, working_directory, text_file):
        self.file_names = []
        self.series_names = []
        self.mappings = defaultdict(list)
        self.working_directory = working_directory
        self.text_file = text_file

    def read_series_names(self):
        """
        Reads a file with the series names line for line
        :param file_name:
        :return: void
        """
        with open(self.text_file) as text_file:
            self.series_names = text_file.read().splitlines()

    def read_working_directory(self):
        """
        Walking the working directory and reads the files
        and adds them to the file_names if there are not series
        foldrs
        :return: void
        """
        directory_content = os.listdir(self.working_directory)
        for content in directory_content:
            if self.not_in_series_name_case_insensitive(content):
                self.file_names.append(content)
            else:
                print("ignored: ", content, " because it's a series folder")

    def not_in_series_name_case_insensitive(self, name):
        """
        Checks whether name is a series name
        :param name:
        :return: bool
        """
        for series_name in self.series_names:
            if series_name.lower() == name.lower():
                return False
        return True

    def find_best_matching_series_name(self, file_name: str):
        """
        Finds the best matching series name for the given file
        :param file_name:
        :return:
        """
        if file_name == "":
            return ""
        if self.series_names.__contains__(file_name):
            return ""
        else:
            return self.find_best_match(file_name)

    def find_best_match(self, file_name: str):
        """
        Adds matches to a list and later return the match
        either the match is "" or the series name it matched best
        :param file_name:
        :return: matches[0], the match
        """
        matches = []
        for name in self.series_names:
            self.match_tokens(file_name, matches, name)
        return self.return_match(matches)

    def match_tokens(self, file_name: str, matches, series_name: str):
        """
        Matches the tokens of the file name to the series name, only
        adds a series if all tokens are in the file name
        :param file_name:
        :param matches:
        :param series_name:
        :return:
        """
        tokens = series_name.split()
        max_tokens = tokens.__len__()
        matched_tokens = 0
        for token in tokens:
            if file_name.lower().__contains__(token.lower()):
                matched_tokens += 1
            if max_tokens == matched_tokens:
                matches.append(series_name)

    def return_match(self, matches):
        """
        Return the longest sequencing match
        :param matches:
        :return:
        """
        if matches.__len__() > 0:
            matches.sort(key=lambda s: len(s), reverse=True)
            return matches[0]
        else:
            return ""

    def map_series_to_files(self):
        """
        Maps the series files
        :return: None
        """
        for file in self.file_names:
            best_match = self.find_best_matching_series_name(file)
            if best_match != "":
                self.mappings.setdefault(best_match, []).append(file)

    def create_series_folders(self):
        """
        Creates folders for all series names
        :return: None
        """
        for name in self.series_names:
            folder_path = os.path.join(self.working_directory, name)
            if os.path.exists(folder_path):
                print("not created: ", folder_path, " reason: already exists")
            else:
                os.mkdir(folder_path)

    # TODO Implement Issue Create Season folder #3
    # TODO rework this need to actually create the season folders in the series name folders
    def create_season_folders(self):
        """
        Creates season folders in the
        :return: None
        """
        # TODO refactor the first loop --> is unnecessarily O(n^2)
        for series_name in self.mappings.keys():
            series_folder_path = os.path.join(self.working_directory, series_name)
            series_numbers = self.find_matching_season_number(series_name)
            for number in series_numbers:
                print(os.path.join(series_folder_path, number.__str__()))

    # TODO Implement Issue Create Season folder #3
    def find_matching_season_number(self, series_name):
        """
        Find the season number which matches the series_name if the series name is
        a key in the mapppings dict
        :param series_name: the series name from which the season number should be extracted
        :return: series_numbers, a list which contains all the matching seriesnumber e.g ['Season 1'. 'Season 2', 'Season 4']
        """
        series_numbers =[]
        key_exists = not self.mappings.get(series_name) is None
        if key_exists:
            for value in self.mappings.get(series_name):
                self.find_match_for_series_regex(series_numbers, value)
            return series_numbers
        else:
            return []

    # TODO Implement Issue Create Season folder #3
    def find_match_for_series_regex(self, series_numbers, value):
        """
        Finds a match for the series regex
        :param series_numbers:
        :param value:
        :return:
        """
        search = re.search(RegexManager.series__number_regex, value)
        match_found = search is not None
        if match_found:
            match = search.group(0)
            match = match.replace("s", "Season ")
            match = re.sub(RegexManager.leading_zeros, r'\1', match)
            if not series_numbers.__contains__(match):
                series_numbers.append(match)

    def print_series_names(self):
        for name in self.series_names:
            print(name)

    def print_files(self):
        for name in self.file_names:
            print(name)

    def move_files_to_series_folders(self):
        """
        Moves the files to the mapped series names folder

        :return: None
        """
        for folder in self.mappings.keys():
            for file in self.mappings.get(folder):
                src = os.path.join(self.working_directory, file)
                dest = os.path.join(self.working_directory, folder, file)
                if os.path.exists(src) and self.not_in_series_name_case_insensitive(file):
                    print("move ", src, " to ", dest)
                    shutil.move(src, dest)


if __name__ == '__main__':
    working_directory = "/media/almynic/BT-Downloads"
    series_text_file = "series_names.txt"
    manager = SeriesManager(working_directory, series_text_file)
    manager.read_series_names()
    manager.create_series_folders()
    manager.read_working_directory()
    manager.map_series_to_files()
    manager.move_files_to_series_folders()
