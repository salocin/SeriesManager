import unittest
from SeriesManager.seriesmanager import *


class SeriesManagerTest(unittest.TestCase):
    working_directory = ""
    text_file = ""
    manager = SeriesManager(working_directory, text_file)

    # TODO write more tests
    def setUp(self):
        self.manager.series_names = ["Hawaii Five-0",
                                "NCIS",
                                "NCIS Los Angeles",
                                "NCIS New Orleans"]
        self.manager.file_names = ["Hawaii.Five-0.s01e01.LOL.HDTV.mkv",
                              "Hawaii.Five-0.s01e02.LOL.HDTV.mkv",
                              "Hawaii.Five-0.s01e01.LOL.HDTV.mkv",
                              "NCIS.Los.Angeles.s03e02.DIMENSION.x264.avi",
                              "NCIS.Los.Angeles.s03e03.DIMENSION.x264.avi",
                              "NCIS.s03e02.DIMENSION.x264.avi",
                              "NCIS.s03e03.DIMENSION.x264.avi",
                              "NCIS.s10e03.DIMENSION.x264.avi"]
        self.manager.mappings = {}
        self.manager.map_series_to_files()

    def test_finds_best_matching_series_correctly(self):
        self.assertEqual("", self.manager.find_best_matching_series_name(""))
        self.assertEqual("", self.manager.find_best_matching_series_name("Hawaii Five-0"))
        self.assertEqual("Hawaii Five-0", self.manager.find_best_matching_series_name("Hawaii Five-0 S02E04 LOL"))
        self.assertEqual("NCIS Los Angeles", self.manager.find_best_matching_series_name("NCIS Los Angeles S02E04 LOL"))
        self.assertEqual("NCIS New Orleans", self.manager.find_best_matching_series_name("NCIS New Orleans S02E04 LOL"))
        self.assertEqual("NCIS Los Angeles", self.manager.find_best_matching_series_name("NCIS.Los.Angeles.S02E04.LOL.avi"))
        self.assertEqual("", self.manager.find_best_matching_series_name("Lucifer.S03E04.LOL.mkv"))

    def test_matching_series_to_files_correctly(self):
        self.assertEqual(["NCIS.Los.Angeles.s03e02.DIMENSION.x264.avi",
                          "NCIS.Los.Angeles.s03e03.DIMENSION.x264.avi"], self.manager.mappings.get("NCIS Los Angeles"))
        self.assertEqual(["NCIS.s03e02.DIMENSION.x264.avi",
                          "NCIS.s03e03.DIMENSION.x264.avi",
                          "NCIS.s10e03.DIMENSION.x264.avi"], self.manager.mappings.get("NCIS"))
        self.assertEqual(["Hawaii.Five-0.s01e01.LOL.HDTV.mkv",
                          "Hawaii.Five-0.s01e02.LOL.HDTV.mkv",
                          "Hawaii.Five-0.s01e01.LOL.HDTV.mkv"], self.manager.mappings.get("Hawaii Five-0"))


    def test_should_return_all_series_numbers_correctly(self):
        self.assertEqual(['Season 1'], self.manager.find_matching_season_number("Hawaii Five-0"))
        self.assertEqual(['Season 3'], self.manager.find_matching_season_number("NCIS Los Angeles"))
        self.assertEqual(['Season 3', 'Season 10'], self.manager.find_matching_season_number("NCIS"))
        self.assertEqual([], self.manager.find_matching_season_number("NCIS New Orleans"))


if __name__ == '__main__':
    unittest.main()
