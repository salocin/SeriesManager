import sys

from PyQt5.QtCore import pyqtSlot, QRunnable, QThreadPool
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import (QApplication, QDialog,
                             QGridLayout, QGroupBox, QLineEdit, QPushButton, QVBoxLayout, QFileDialog, QMessageBox)

from SeriesManager.seriesmanager import SeriesManager


class App(QDialog):

    def __init__(self):
        super(App, self).__init__()
        self.appMinWidthAndHeight = 500
        self.initalizeAppVariables()
        self.intializeAppLayout()
        self.createAppUI()
        self.show()

    def initalizeAppVariables(self):
        self.maxHeightGroupBoxElement = 30
        self.groupboxMainGridMaxHeight = 150
        self.maxWidthGroupBoxElement = 350
        self.groupBoxMaxWidth = 450
        self.uiGroupBoxMaxWidth = 200
        self.workingDirectory = ""
        self.textFile = ""

    def intializeAppLayout(self):
        self.resize(self.appMinWidthAndHeight, self.appMinWidthAndHeight)
        self.setMinimumWidth(self.appMinWidthAndHeight)
        self.setMinimumHeight(self.appMinWidthAndHeight)
        self.createSettingsGridLayout()
        self.createMainGridLayout()
        self.setWindowIcon(QIcon('seriesmanager.png'))
        self.setWindowTitle("Series Manager")

    def createAppUI(self):
        layout = QVBoxLayout()
        layout.addWidget(self.settingsGroupBox)
        layout.addWidget(self.managingGroupBox)
        self.setLayout(layout)

    # TODO refactor this method break it up
    # Done partially done
    def createSettingsGridLayout(self):
        self.createLoadFileUI()
        self.createChooseDirectoryUI()
        self.loadFileButton.clicked.connect(self.chooseTextFile)
        self.chooseDirectoryButton.clicked.connect(self.chooseWorkingDirectory)
        self.createSettingsUI()

    def createSettingsUI(self):
        self.settingsGroupBox = QGroupBox("Settings")
        self.settingsGroupBox.setMaximumHeight(self.uiGroupBoxMaxWidth)
        self.settingsGroupBox.setMaximumWidth(self.groupBoxMaxWidth)
        layout = QGridLayout()
        layout.addWidget(self.loadFileButton, 0, 0)
        layout.addWidget(self.loadedFileText, 1, 0)
        layout.addWidget(self.chooseDirectoryButton, 2, 0)
        layout.addWidget(self.choosenDirectoryText, 3, 0)
        self.settingsGroupBox.setLayout(layout)

    def createMainGridLayout(self):
        self.createMainGridGroupBox()
        self.createInitUI()
        self.createMoveUI()
        self.createApplyUI()
        self.createLayout()

    def createMainGridGroupBox(self):
        self.managingGroupBox = QGroupBox("Actions")
        self.managingGroupBox.setMaximumHeight(self.groupboxMainGridMaxHeight)
        self.managingGroupBox.setMaximumWidth(self.groupBoxMaxWidth)

    def createInitUI(self):
        self.initButton = QPushButton("Initalize Series Manager")
        self.initButton.setMaximumWidth(self.maxWidthGroupBoxElement)
        self.initButton.setMaximumHeight(self.maxWidthGroupBoxElement)
        self.initButton.clicked.connect(self.initalizeSeriesManager)

    def createMoveUI(self):
        self.moveEpisodeToSeriesButton = QPushButton("Move episodes to series folders")
        self.moveEpisodeToSeriesButton.setMaximumWidth(self.maxWidthGroupBoxElement)
        self.moveEpisodeToSeriesButton.setMaximumHeight(self.maxWidthGroupBoxElement)
        self.moveEpisodeToSeriesButton.clicked.connect(self.moveFiles)

    def createApplyUI(self):
        self.applyEpisodeFormatButton = QPushButton("Apply episode format in series folders")
        self.applyEpisodeFormatButton.setMaximumWidth(self.maxWidthGroupBoxElement)
        self.applyEpisodeFormatButton.setMaximumHeight(self.maxWidthGroupBoxElement)

    def createLayout(self):
        layout = QGridLayout()
        layout.addWidget(self.initButton, 0, 0)
        layout.addWidget(self.moveEpisodeToSeriesButton, 1, 0)
        layout.addWidget(self.applyEpisodeFormatButton, 2, 0)
        self.managingGroupBox.setLayout(layout)

    def createChooseDirectoryUI(self):
        self.createChooseDirectoryButton()
        self.createChoosenDirectoryText()

    def createChooseDirectoryButton(self):
        self.chooseDirectoryButton = QPushButton("Choose working directory")
        self.chooseDirectoryButton.setMaximumHeight(self.maxHeightGroupBoxElement)
        self.chooseDirectoryButton.setMaximumWidth(self.maxWidthGroupBoxElement)

    def createChoosenDirectoryText(self):
        self.choosenDirectoryText = QLineEdit(self)
        self.choosenDirectoryText.setReadOnly(True)
        self.choosenDirectoryText.setMaximumWidth(self.maxWidthGroupBoxElement)
        self.choosenDirectoryText.setMaximumHeight(self.maxWidthGroupBoxElement)

    def createLoadFileUI(self):
        self.createLoadFileButton()
        self.createLoadedFileText()

    def createLoadFileButton(self):
        self.loadFileButton = QPushButton("Load file with series names")
        self.loadFileButton.setMaximumWidth(self.maxWidthGroupBoxElement)
        self.loadFileButton.setMaximumHeight(self.maxWidthGroupBoxElement)

    def createLoadedFileText(self):
        self.loadedFileText = QLineEdit(self)
        self.loadedFileText.setReadOnly(True)
        self.loadedFileText.setMaximumHeight(self.maxWidthGroupBoxElement)
        self.loadedFileText.setMaximumWidth(self.maxWidthGroupBoxElement)

    @pyqtSlot(name="chooseTextFile")
    def chooseTextFile(self):
        self.openFileNameDialog()

    @pyqtSlot(name="chooseWorkingDirectory")
    def chooseWorkingDirectory(self):
        self.openFolderDialog()

    @pyqtSlot(name="intialize")
    def initalizeSeriesManager(self):
        if self.workingDirectory and self.textFile:
            self.performPreconditionActionsForSeriesManager()
        else:
            self.showSetSettingsWarning()

    def performPreconditionActionsForSeriesManager(self):
        self.manager = SeriesManager(self.workingDirectory, self.textFile)
        self.manager.read_working_directory()
        self.manager.read_series_names()
        self.manager.create_series_folders()
        self.manager.map_series_to_files()

    @pyqtSlot(name="moveFiles")
    def moveFiles(self):
        if self.textFile and self.workingDirectory:
            if self.manager.mappings.__len__() > 0:
                self.moveFilesUsingRunnable()
        else:
            self.showNotIntializedWarning()

    def showSetSettingsWarning(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText("Please set working directory and seris name text file")
        msg.setInformativeText("Go to settings and set the text file and the working directory location")
        msg.setWindowTitle("Settings are not satisfied")
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()

    def showNotIntializedWarning(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText("Please initalize series manager first")
        msg.setInformativeText("Click on initalize series manager")
        msg.setWindowTitle("Series manager is not initalized")
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()

    def openFileNameDialog(self):
        options = QFileDialog.Options()
        fileName, _ = QFileDialog.getOpenFileName(self, "Select text file with series names)", "",
                                                  "Text files (*.txt)", options=options)
        if fileName:
            self.loadedFileText.setText(fileName)
            self.textFile = fileName

    def openFolderDialog(self):
        options = QFileDialog.Options()
        workingDirectory = str(QFileDialog.getExistingDirectory(self, "Select Directory", options=options))
        if workingDirectory:
            self.choosenDirectoryText.setText(workingDirectory)
            self.workingDirectory = workingDirectory

    def moveFilesUsingRunnable(self):
        runnable = MoveFilesRunnable(self.manager)
        QThreadPool.globalInstance().start(runnable)


class MoveFilesRunnable(QRunnable):

    def __init__(self, manager):
        super().__init__()
        self.manager = manager

    def run(self):
        self.manager.move_files_to_series_folders()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
