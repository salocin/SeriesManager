from setuptools import setup

setup(
    name='SeriesManager',
    version='1.0',
    packages=['SeriesManager'],
    install_requires=[''],
    url='',
    license='',
    author='Nicolas Almy',
    author_email='nicolas.almy@nicomania.ch',
    description='A app to manage series'
)
